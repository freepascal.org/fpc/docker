# syntax=docker/dockerfile:1
##
## Configuration
##
# set Debian release version (latest, buster, bullseye, ...)
ARG DEBIAN_VERSION=bookworm
# set FPC version (format: d.d.d)
ARG FPC_VERSION=3.2.2
# set FPC trunk version (format: d.d.d)
ARG FPC_TRUNK_VERSION=3.3.1


##
## Create archive with FPC trunk by compiling source with latest official FPC
##
FROM freepascal/fpc:${FPC_VERSION}-${DEBIAN_VERSION}-full AS builder
WORKDIR /tmp
# docker buildx variables
ARG TARGETPLATFORM
ARG BUILDPLATFORM
# install git, clone sourcecode, compile FPC trunk and create single zip
RUN set -eux \
    && apt-get update \
    && apt-get install -y git \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && echo "Generating single archive installation for ${TARGETPLATFORM} on ${BUILDPLATFORM}." \
    # get sources from git repository
    # git describe does not work so fpc won't show the hash in output
    #&& git clone --branch main --single-branch --depth=1 https://gitlab.com/freepascal.org/fpc/source.git \
    && git clone --branch main https://gitlab.com/freepascal.org/fpc/source.git \
    && cd source* \
    # add git hash to fpc output
    && touch compiler/revision.inc \
    # compile and create single archive
    && make zipinstall -j$(nproc) FPMAKEOPT="-T $(nproc)" \
    && mv fpc-*.tar.gz /opt/fpc-archive.tar.gz \
    # cleanup
    && rm -r /tmp/*


##
## Full installation of FPC trunk
##
# reuse previous argument value
ARG DEBIAN_VERSION
FROM debian:${DEBIAN_VERSION} AS fpc-full
WORKDIR /ws
# reuse previous argument value
ARG FPC_TRUNK_VERSION
# docker buildx variables
ARG TARGETPLATFORM
ARG BUILDPLATFORM
# copy created archive
COPY --from=builder /opt/fpc-archive.tar.gz /ws
# update system and install FPC
RUN set -eux \
    && apt-get update \
    && apt-get -y upgrade \
    && apt-get install -y build-essential \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && echo "Installing FPC trunk for ${TARGETPLATFORM} on ${BUILDPLATFORM}." \
    # extract compiled development version
    && tar -xzf fpc-archive.tar.gz \
    # install files
    && cp -r bin/* /usr/bin/ \
    && cp -r lib/* /usr/lib/ \
    && cp -r share /usr/share/doc/ \
    # generate config and symlink
    && fpcmkcfg -d basepath="/usr/lib/fpc/\$fpcversion" -o /etc/fpc.cfg \
    && ln -s $(find /usr/lib/fpc/${FPC_TRUNK_VERSION}/ -name ppc*) /usr/bin/$(basename $(find /usr/lib/fpc/${FPC_TRUNK_VERSION}/ -name ppc*)) \
    # cleanup
    && rm -r /ws/*
